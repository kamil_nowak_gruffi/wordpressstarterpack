# README #

## Setup instructions ##

* Make sure you have npm and nodejs installed, and that you installed gulp globally with npm (npm install gulp -g)
* Open terminal in project's main directory:
* cd front
* open gulpfile and set templateName (it has to be the same as your wordpress theme folder name in wp-content/themes)
* open package.json and set the name of App
* npm install (or better - yarn install)
* npm start <-- to watch files, npm run build <-- to rebuild all files


## Wordpress theme config

* open wp-content/themes/
* rename templateName to your template/project name
* edit style.css and set theme names etc.
* open functions.php and replace 'projectname' and 'projectName' with your project name
